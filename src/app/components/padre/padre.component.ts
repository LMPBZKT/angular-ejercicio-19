import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {
  personaPadre!: any;
  mascotaAnimal!: any;

  constructor() { }

  ngOnInit(): void {
  }

  Objeto($event: any): void {
    this.personaPadre = $event;
    console.log(this.personaPadre);
  }

  Objeto2($event: any): void {
    this.mascotaAnimal = $event;
    console.log(this.mascotaAnimal);

  }
}
