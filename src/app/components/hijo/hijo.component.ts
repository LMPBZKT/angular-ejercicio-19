import { Component, OnInit } from '@angular/core';
import { Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  Persona ={
    nombre:'Edwin',
    apellidos: 'Errera',
    edad: 50,
    sexo: 'Masculino',
    estado: 'Casado',
    mensaje: 'Objeto1 del hijo al padre'
  }

  Mascota ={
    Nombre:'Juan Pablo',
    Edad:'2 anos',
    Raza:'Bulldog',
    Color: 'Cafe',
    Mensaje: 'Objeto2 del hijo al padre'
  }

  @Output() Emitir1 = new EventEmitter<any>();
  @Output() Emitir2 = new EventEmitter<any>();
  constructor() { }

  ngOnInit(): void {
    this.Emitir1.emit(this.Persona);
    this.Emitir2.emit(this.Mascota)
  }

}
